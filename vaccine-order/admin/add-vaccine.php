<?php include('partials/list.php');?>
<div class="main-content">
	<div class="wrapper">
		<h1>Add Vaccine</h1>

		<br><br>

		<?php
		if(isset($_SESSION['upload']))
		{
			echo $_SESSION['upload'];
			unset($_SESSION['upload']);
		}

		?>

		<form action="" method="post" enctype="multipart/form-data">

			<table class="tbl-30">
				<tr>
					<td>Title: </td>
					<td>
						<input type="text" name="title" placeholder="Title of Vaccine">
					</td>
				</tr>
				<tr>
					<td>Description: </td>
					<td>
						<textarea name="description"  cols="30" rows="5" placeholder="Descriptio of vaccine"></textarea>
					</td>
				</tr>
				<tr>
					<td>Price: </td>
					<td>
						<input type="number" name="price">
					</td>
				</tr>
				<tr>
					<td>Select Image: </td>
					<td>
						<input type="file" name="image">
					</td>
				</tr>
				<tr>
					<td>Featured: </td>
					<td>
						<input type="radio" name="featured" value="Yes">Yes
						<input type="radio" name="featured" value="No">No
					</td>
				</tr>
				<tr>
					<td>Active: </td>
					<td>
						<input type="radio" name="active" value="Yes">Yes
						<input type="radio" name="active" value="No">No
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="submit" value=" Add Vaccine" class="btn-secondary">

					</td>
				</tr>

			</table>
	
		
	
		</form>

		<?php
		// chec wetheer button is clicked
		if(isset($_POST['submit']))
		{
			//echo "clicked?";
			//get data from form
			$title=$_POST['title'];
			$description=$_POST['description'];
			$price=$_POST['price'];

			//check wether radio buttons checked
			if(isset($_POST['featured']))
			{
				$featured= $_POST['featured'];
			}
			else
			{
				$featured="No";//default value
			}

			if(isset($_POST['active']))
			{
				$active=$_POST['active'];
			}
			else
			{
				$active="No";
			}

			//upload image if selected
			if(isset($_FILES['image']['name']))

			{
				//details of image
				$image_name=$_FILES['image']['name'];

				//check if image is selected and upload only when selected
				if($image_name=="")
				{
					//image is selected
					//rename image
					//get extension of selected image
					$ext = end(explode('.', $image_name));
					 
					//create new namw dor image
					$image_name = "vaccine".rand(0000,9999).".".$ext;

					$src = $_FILES['image']['tmp_name'];

					$dst= "../images/".$image_name;

					$upload= move_uploaded_file($src, $dst);

					//checj wether image uploaded or not
					if(upload==false)
					{
						$_SESSION['upload']="<div class='error'>Failed to uploaad image.</div>";
						header('location:'.SITEURL.'admin/add-vaccine.php');

						die(); 
					}
				}
				

			}
			else
			{
				$image_name="";

			}

			//insert intodatabase
			$sql2 = "INSERT INTO tbl_vaccine SET
			title='$title',
			description='$description',
			price=$price,
			image_name='$image_name',
			featured='$featured',
			active='$active'
			
			";
			//execute queery

			$res2 = mysqli_query($conn, $sql2);

			if($res2 == true)
			{
				//data inserted succesfully
				$_SESSION['add'] = "<div class='success'> Vaccine added successfully</div>";
				header('location:'.SITEURL.'admin/manage-vaccine.php');
			}
			else
			{
				$_SESSION['add'] = "<div class='error'>Failed to add vaccine</div>";
				header('location:'.SITEURL.'admin/manage-vaccine.php');

			}

		}
		?>

	</div>
</div>



<?php include('partials/footer.php');?>
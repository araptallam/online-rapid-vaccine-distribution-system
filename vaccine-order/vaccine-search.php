<?php include('partials-front/nav.php'); ?>

    <!-- vaccine list Section Starts Here -->
    <section class="vaccine-search text-center">
        <div class="container">
            
        <?php
         //get search keyword
            $search = mysqli_real_escape_string($conn, $_POST['search']);

        ?>

            <h2 class="text-white">Vaccines on Your Search <a href="vaccines.html" class="text-white">"<?php echo $search; ?>"</a></h2>

        </div>
    </section>
    <!-- vaccine Section Ends Here -->



    <!-- vaccine list Section Starts Here -->
    <section class="list">
        <div class="container">
            <h2 class="text-center">List</h2>

            <?php
           

            //sql query to get vaccines based on search

            $sql = "SELECT * FROM tbl_vaccine WHERE title LIKE '%$search%' OR description LIKE '%$search%'";

            //execute the query
            $res = mysqli_query($conn, $sql);

            //count rows
            $count = mysqli_num_rows($res);

            //check wether vaccine is available o not
            if($count>0)
            {
                //vaccine available
                while($row=mysqli_fetch_assoc($res))
                {
                    //get details
                    $id = $row['id'];
                    $title = $row['title'];
                    $price = $row['price'];
                    $description = $row['description'];
                    $image_name = $row['image_name'];

                    ?>
                        <div class="list-box">
                            <div class="list-img">
                                <?php
                                    //check wether image is available or not
                                    if($image_name=="")
                                    {
                                        //image not available
                                        echo "<div class='error'>Image not Available</div>";
                                    }
                                    else
                                    {
                                        //image available
                                        // <img src="images/malaria.jpg" alt="Malaria vaccine" class="img-responsive img-curve">
                                        ?>
                                        
                                          <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Polio" class="img-responsive img-curve">
                                        <?php
                                    }

                                ?>
                               
                            </div>

                            <div class="description">
                                <h4><?php echo $title; ?></h4>
                                <p class="price">$<?php echo $price; ?></p>
                                <p class="detail">
                                    <?php echo $description; ?>
                                </p>
                                <br>

                              <a href="<?php echo SITEURL; ?>order.php?vaccine_id=<?php echo $id; ?>" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>

                    <?php
                }
            }
            else
            {
                //vaccine not avaolable
                echo "<div class='error'>Vaccine Not Found.</div>";
            }

            ?>


           <div class="clearfix"></div>



        </div>

    </section>
    <!-- vaccine search Section Ends Here -->

   <?php include('partials-front/footer.php'); ?>
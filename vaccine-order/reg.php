<?php include('config/constants.php'); ?>
<!DOCTYPE html>
<!-- Created By CodingLab - www.codinglabweb.com -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <!---<title> Responsive Registration Form | CodingLab </title>--->
    <link rel="stylesheet" href="css/reg.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>

   	


<body>
  <div class="container">
    <div class="title">Registration</div>
    <div class="content">
      <form action=""  method="post" enctype="multipart/form-data">
        <div class="user-details">
          <div class="input-box">
            <span class="details">National ID</span>
            <input type="text" name="id" placeholder="ID number" required>
          </div>
	  <div class="input-box">
            <span class="details">Full Name</span>
            <input type="text" name="name" placeholder="Enter your name" required>
          </div>
          <div class="input-box">
            <span class="details">Username</span>
            <input type="text" name="username" placeholder="Enter your username" required>
          </div>
          <div class="input-box">
            <span class="details">Email</span>
            <input type="text" name="email" placeholder="Enter your email" required>
          </div>
          <div class="input-box">
            <span class="details">Phone Number</span>
            <input type="text" name="number" placeholder="Enter your number" required>
          </div>
          <div class="input-box">
            <span class="details">Password</span>
            <input type="text" name="password" placeholder="Enter your password" required>
          </div>
          
        </div>
        <div class="gender-details">
          <input type="radio" name="gender" id="dot-1">
          <input type="radio" name="gender" id="dot-2">
          
          <span class="gender-title">Gender</span>
          <div class="category">
            <label for="dot-1">
            <span class="dot one"></span>
            <span class="gender">Male</span>
          </label>
          <label for="dot-2">
            <span class="dot two"></span>
            <span class="gender">Female</span>
          </label>
          <label for="dot-3">
          
          </div>
        </div>
        <div class="button">
          <input type="submit" name="submit" value="Register">
        </div>
      </form>

      <?php
      if(isset($_POST['submit']))
	{
		//echo "button";
		$id=$_POST['id'];
		$name= $_POST['name'];
		$username= $_POST['username'];
		$email= $_POST['email'];
		$number= $_POST['number'];
		$password= md5($_POST['password']);
	

		if(isset($_POST['gender']))
		{
			$gender=$_POST['gender'];
		}
		else
		{
			$gender="dot-2";
		}

		//insert into database
		$sql=" INSERT INTO tbl_users SET
		id='$id',
		name='$name',
		username='$username',
		email='$email',
		number='$number',
		password='$password'
		
		";

		//execute query
		$res = mysqli_query($conn, $sql);

		if($res == true)
		{
				//data inserted succesfully
				$_SESSION['add'] = "<div class='success text-center'> Registration successful</div>";
				header('location:'.SITEURL.'categories.php');
		}
		else
		{
				$_SESSION['add'] = "<div class='error text-center'>Registration not successful</div>";
				header('location:'.SITEURL.'categories.php');

		}
        }
      ?>
    </div>
  </div>

</body>
</html>




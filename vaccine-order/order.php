<?php include('partials-front/nav.php'); ?>

<?php
    //check wether vaccne id is set or not
    if(isset($_GET['vaccine_id']))
    {
        //id is set
        $vaccine_id = $_GET['vaccine_id'];
        //get deails of selected vaccine
        $sql = "SELECT * FROM tbl_vaccine WHERE id=$vaccine_id";
        //execute query
        $res = mysqli_query($conn, $sql);
        //count the rows
        $count = mysqli_num_rows($res);
        //ceck wether data is available or not
        if($count==1)
        {
            //we have data
            //get the data
            $row = mysqli_fetch_assoc($res);
            $title = $row['title'];
            $price = $row['price'];
            $image_name = $row['image_name'];
        }
        else
        {
            //no data
            //redirect
            header('location:'.SITEURL);
        }
    }
    else
    {
        //redirect to homepage
        header('location:'.SITEURL);
    }
?>

    <!-- vaccine sEARCH Section Starts Here -->
    <section class="vaccine-search">
        <div class="container">

            <h2 class="text-center text-white">Fill this form to confirm your order.</h2>

            <form action="" method="POST" class="order">
                <fieldset>
                    <legend>Selected Vaccine</legend>

                    <div class="list-img">
                        <?php
                            //check wether image is available or not
                            if($image_name=="")
                            {
                                //image not available
                                echo "<div class='error'>Image Not Available</div>";
                            }
                            else
                            {
                                //image avaialabel
                                 //<img src="images/malaria.jpg" alt="Selected vaccine" class="img-responsive img-curve">
                                ?>
                                    <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Polio" class="img-responsive img-curve">
                                <?php
                            }

                            
                        ?>
                       
                    </div>

                    <div class="description">
                        <h3><?php echo $title; ?></h3>
                        <input type="hidden" name="vaccine" value="<?php echo $title; ?>">

                        <p class="price">$<?php echo $price; ?></p>
                        <input type="hidden" name="price" value="<?php echo $price; ?>">

                        <div class="order-label">Quantity</div>
                        <input type="number" name="qty" class="input-responsive" value="1" required>

                    </div>

                </fieldset>

                <fieldset>
                    <legend>Delivery Details</legend>
                    <div class="order-label">Full Name</div>
                    <input type="text" name="full-name" placeholder="E.g. Vijay Thapa" class="input-responsive" required>

                    <div class="order-label">Phone Number</div>
                    <input type="tel" name="contact" placeholder="E.g. 9843xxxxxx" class="input-responsive" required>

                    <div class="order-label">Email</div>
                    <input type="email" name="email" placeholder="E.g. hi@vijaythapa.com" class="input-responsive" required>

                    <div class="order-label">Address</div>
                    <textarea name="address" rows="10" placeholder="E.g. Street, City, Country" class="input-responsive" required></textarea>

                    <input type="submit" name="submit" value="Confirm Order" class="btn btn-primary">
                </fieldset>

            </form>

            <?php

            //check wether submit button is clicked or not
            if(isset($_POST['submit']))
            {
                //get details from form
                $vaccine = $_POST['vaccine'];
                $price = $_POST['price'];
                $qty = $_POST['qty'];

                $total = $price * $qty; 

                $order_date = date("Y-m-d h:i:sa"); // order date

                $status = "Ordered";

                $customer_name = $_POST['full-name'];
                $customer_contact = $_POST['contact'];
                $customer_email = $_POST['email'];
                $customer_address = $_POST['address'];

                //save order in db
                //create sql to save data
                $sql2 = "INSERT INTO tbl_order SET
                    vaccine = '$vaccine',
                    price = $price,
                    qty = $qty,
                    total = $total,
                    order_date = '$order_date',
                    status = '$status', 
                    customer_name = '$customer_name',
                    customer_contact = '$customer_contact',
                    customer_email = '$customer_email',
                    customer_address = '$customer_address'

                ";
                
                //execute the query
                $res2 = mysqli_query($conn, $sql2);

                //check wether query executed
                if($res2==true)
                {
                    //query executed and order saved
                    $_SESSION['order'] = "<div class='success text-center'>Vaccine Ordered Successfully.</div>";
                    header('location:'.SITEURL);
                }
                else
                {
                    //failed to save order
                     $_SESSION['order'] = "<div class='error text-center'>Failed To Order Vaccine.</div>";
                    header('location:'.SITEURL);
                }

            }

            ?>

        </div>
    </section>
    <!-- vaccine sEARCH Section Ends Here -->

  <?php include('partials-front/footer.php'); ?>
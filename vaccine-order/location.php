<?php include('partials-front/nav.php'); ?>
<!DOCTYPE html>
<html>
	<head>
	<title>Geo-location</title>
	<style>
		
		#map{
			height: 600px;
			width: 80%;
			display: block;
 			 margin-left: auto;
 			 margin-right: auto;
		}
	</style>
	</head>
	<body>
		<div id="map"></div>
		<script>
  function initMap () {
    var location = {lat: -1.292066, lng: 36.821945};
    var map = new google.maps.Map(document.getElementById("map"),{ zoom: 4, center: location});

    var marker = new google.maps.Marker({position: location, map: map});
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAe_47ZBuTJYhtOW9e-sYpbO9KWQSDhWoU&callback=initMap"></script> 

	</body>
</html>
  <?php include('partials-front/footer.php'); ?>
<?php include('partials-front/nav.php'); ?>
    <section>
         <div class="container text-center">
            <section>
                <h2>Welcome To Rapid Online Vaccine Distribution System</h2>
                <h1>Here we try to bridge the gap between you and the vaccine services that you need.The vaccines that you order
                    will be delivered to your prefered address upon confirmation of all details.
                     Please make sure you go through the immunization schedule before placing any orders.
          </h1>

            </section>
        </div>
        <br>
    </section>
    <!-- Navbar Section Ends Here -->
    

    <!-- Vaccine sEARCH Section Starts Here -->
    <section class="vaccine-search text-center">
        <div class="container">

            <form action="<?php echo SITEURL; ?>vaccine-search.php" method="POST">
                <input type="search" name="search" placeholder="Search for Vaccines.." required>
                <input type="submit" name="submit" value="Search" class="btn btn-primary">
            </form>

        </div>
    </section>
    <br>
    <!-- vaccine sEARCH Section Ends Here -->
    

    <?php
    if(isset($_SESSION['order']))
    {
        echo $_SESSION['order'];
        unset ($_SESSION['order']);
    }
    ?>
    <?php
		if(isset($_SESSION['add']))
		{
			echo $_SESSION['add'];
			unset($_SESSION['add']);
		}

	?>

    <!-- CAtegories Section Starts Here -->
    <section class="categories">
        <div class="container">
            <h2 class="text-center">Explore Vaccines</h2>

            <?php 
            //create sql query to display categories
            $sql = "SELECT * FROM tbl_category WHERE active='Yes' AND featured='Yes' LIMIT 3";
            //execute the query
            $res = mysqli_query($conn, $sql);
            //count  rows to check wether category is availabale or not
            $count = mysqli_num_rows($res);
            
            if($count>0)
            {
                //categories available
                while($row=mysqli_fetch_assoc($res))
                {
                    //get values
                    $id=$row['id'];
                    $title=$row['title'];
                    $image_name=$row['image_name'];
                    ?>
                           <a href="<?php echo SITEURL; ?>category-vaccines.php?category_id=<?php echo $id; ?>">
                                <div class="box-3 float-container">
                                    <?php
                                    //check availability of image
                                    if($image_name=="")
                                    {
                                        //display image
                                        echo "<div class='error'>Image Not Available</div>";
                                    }
                                    else
                                    {
                                        //image available
                                        ?>
                                         <img src="<?php echo SITEURL; ?>images/category/<?php echo $image_name;?>" alt="Malaria" class="img-responsive img-curve">

                                        <?php
                                    }
                                    ?>
                                   
                                    <h3 class="float-text text-white"><?php echo $title;?></h3>
                                </div>
                            </a>

                    <?php
                }
            }
            else
            {
                //categories not available
                echo "<div class='error'>Category not Added.</div>";
            }
            
            ?>

            <div class="clearfix"></div>
        </div>
    </section>
    <!-- Categories Section Ends Here -->

    
    <!-- vaccine in store Section Starts Here -->
    <section class="list" style="background-color: rgb(197, 209, 223)";>
        <div class="container">
            <h2 class="text-center">Available Vaccines</h2>


            <?php
            //getting vaccines from database
            //sql query
            $sql2 = "SELECT * FROM tbl_vaccine WHERE active='Yes' AND featured='Yes' LIMIT 8";

            //EXECUTE QUERY
            $res2 = mysqli_query($conn, $sql2);

            //count rows
            $count2 = mysqli_num_rows($res2);

            //check vaccine availability

            if($count2>0)
            {
                //vaccine avalable
                while($row=mysqli_fetch_assoc($res2))
                {
                    //get all the values
                    $id = $row['id'];
                    $title = $row['title'];
                    $price = $row['price'];
                    $description = $row['description'];
                    $image_name = $row['image_name'];

                    ?>
                        <div class="list-box">
                                <div class="list-img">

                                    <?php
                                    //check wether image is available or not
                                        if($image_name=="")
                                        {
                                            //image not available
                                            echo "<div class='error'>Image not available</div>";
                                        }
                                        else
                                        {
                                            //image available
                                            ?>
                                             <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Polio" class="img-responsive img-curve">

                                            <?php
                                        }

                                    ?>

                                    
                                </div>

                                <div class="description">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="price">$ <?php echo $price; ?></p>
                                    <p class="detail">
                                        <?php echo $description;?>
                                    </p>
                                    <br>

                                    <a href="<?php echo SITEURL; ?>order.php?vaccine_id=<?php echo $id; ?>" class="btn btn-primary">Order Now</a>
                                </div>
                        </div>


                    <?php
                
                }
                 
            }
            else
            {
                //vaccine not available
                echo "<div class='error'>Vaccine not available</div>";
            }

            ?>

               
            <div class="clearfix"></div>

        </div>

        <p class="text-center">
            <a href="vaccines.php">See All Vaccines</a>
        </p>
    </section>
    <!-- vaccine Section Ends Here -->

   <?php include('partials-front/footer.php'); ?>
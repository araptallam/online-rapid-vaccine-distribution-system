<?php include('partials-front/nav.php'); ?>
 <section>
         <div class="container text-center">
            <section>
                <h2>Rapid Online Vaccine Distribution System</h2>


            </section>
        </div>
    </section>

    <!-- Vaccne sEARCH Section Starts Here -->
    <section class="vaccine-search text-center">
        <div class="container">

            <form action="<?php echo SITEURL; ?>vaccine-search.php" method="POST">
                <input type="search" name="search" placeholder="Search for Vaccine.." required>
                <input type="submit" name="submit" value="Search" class="btn btn-primary">
            </form>

        </div>
    </section>
    <!-- vaccine sEARCH Section Ends Here -->



    <!-- vaccine available Section Starts Here -->
    <section class="list" style="background-color: white;">
        <div class="container">
            <h2 class="text-center">Available Vaccines</h2>

            <?php
            //display active vaccines
            $sql = "SELECT * FROM tbl_vaccine WHERE active='Yes'";

            //execute query
            $res = mysqli_query($conn, $sql);

            //count rows
            $count = mysqli_num_rows($res);

            //check if vaccine is availbale
            
            if($count>0)
            {
                //vaccine availabe
                while($row=mysqli_fetch_assoc($res))
                {
                    //get values
                    $id = $row['id'];
                    $title = $row['title'];
                    $description = $row['description'];
                    $price = $row['price'];
                    $image_name = $row['image_name'];

                    ?>
                      <div class="list-box">
                            <div class="list-img">
                                <?php
                                //check wether image is available
                                if($image_name=="")
                                {
                                    //image not available
                                    echo "<div class='error'>Image not available</div>";
                                }
                                else
                                {
                                    //image available
                                    ?>
                                    <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Polio" class="img-responsive img-curve">

                                    <?php
                                }

                                ?>
                                
                            </div>

                            <div class="description">
                                <h4><?php echo $title; ?></h4>
                                <p class="price">$<?php echo $price; ?></p>
                                <p class="detail">
                                   <?php echo $description; ?>
                                </p>
                                <br>

                                <a href="<?php echo SITEURL; ?>order.php?vaccine_id=<?php echo $id; ?>" class="btn btn-primary">Order Now</a>
                         </div>
            </div>


                    <?php
                }
            }
            else
            {
                echo "<div class='error'>Vaccine not found</div>";
            }
            ?>
     


            <div class="clearfix"></div>



        </div>

    </section>
    <!-- vaccine list Section Ends Here -->

<?php include('partials-front/footer.php'); ?>   
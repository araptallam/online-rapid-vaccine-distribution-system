<?php include('partials-front/nav.php'); ?>

    <?php
        //chek if id is passed or not
        if(isset($_GET['category_id']))
        {
            //category id is set get id
            $category_id= $_GET['category_id'];

            //get the category titke based on id
            $sql = "SELECT title FROM tbl_category WHERE id=$category_id";

            //execute query
            $res= mysqli_query($conn, $sql);

            //get value from database
            $row= mysqli_fetch_assoc($res);

            //get the title
            $category_title = $row['title'];
        }
        else
        {
            //category not paased
            //redirect to homepage
            header('location:'.SITEURL);
        }

    ?>

    <!-- fOOD sEARCH Section Starts Here -->
    <section class="vaccine-search text-center">
        <div class="container">

            <h2>Vaccines on <a href="#" class="text-white">"<?php echo $category_title; ?>"</a></h2>

        </div>
    </section>
    <!-- fOOD sEARCH Section Ends Here -->



    <!-- fOOD MEnu Section Starts Here -->
    <section class="list">
        <div class="container">
            <h2 class="text-center">List of Vaccines in category</h2>

            <?php
                //sql to get vaccines based on category
                $sql2 = "SELECT * FROM tbl_vaccine WHERE category_id=$category_id";
                
                //execute query
                $res2 = mysqli_query($conn, $sql2);

                //count rows
                $count2 = mysqli_num_rows($res2);

                //check wether vaccines is available or not
                if($count2>0)
                {
                    //vaccines
                    while($row2=mysqli_fetch_assoc($res2))
                    {
                        $id = $row2['id'];
                        $title = $row2['title'];
                        $price = $row2['price'];
                        $description = $row2['description'];
                        $image_name = $row2['image_name'];

                        ?>
                         <div class="list-box">
                            <div class="list-img">

                            <?php
                            if($image_name=="")
                            {
                                //image not available
                                echo "<div class='error'>Image Not Available</div>";
                            }
                            else
                            {
                                //image available
                                 //<img src="images/polio 3.jpg" alt="Polio" class="img-responsive img-curve">
                                ?>
                                <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Polio" class="img-responsive img-curve">


                                <?php
                            }


                            ?>

                            
                               
                            </div>

                            <div class="description">
                                <h4><?php echo $title; ?></h4>
                                <p class="price">$<?php echo $price; ?></p>
                                <p class="detail">
                                    <?php echo $description; ?>
                                </p>
                                <br>

                                <a href="<?php echo SITEURL; ?>order.php?vaccine_id=<?php echo $id; ?>" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>

                        <?php
                    }
                }
                else
                {
                    //vaccines not availavel
                    echo "<div class='error'>Vaccines Not Found.</div>";
                }



            ?>

           

         
            <div class="clearfix"></div>



        </div>

    </section>
    <!-- fOOD Menu Section Ends Here -->

   <?php include('partials-front/footer.php'); ?>